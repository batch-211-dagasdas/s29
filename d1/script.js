/*
	MongoDB -  Query Operators
	expand Queries in MongoDB using Query operators
*/ 
/*
	OverView:
	-Query Operator
		-Definition 
		-Importance
	Type of Query Operators:
		-Comparison Query Operators
			-Greater than
			-Greater than or equal to
			-less Than
			-less that or equal to 
			-not equal to
			-In
		-Evaluation Query Operators
			-Regex
				-case Sensitive Query
				-Case insensitive querys
		-Logical Query Operators
			-OR
			-And
		-Field Projection
			-Inclution
				-returning specific fields in embedded documents
				-exception to the inclusion rule
				-Slice Operators
			-Exclusio
				-Excluding specific field in embedded document

*/

/*
	what does query Operator mean?
	-A "Query" is a request for data from Database.
	-An "Operator" is Symbol that represents an action or a process
	-Putting them together, they mean the thing that we can do on our queries using certain operators
*/
/*
	Why do we need to study Query Operators?
		-Knowing Query Operators will enable us to create queries that can do more that what simple operators do (we can do more that what we do in simple CRUD operators )
		-For example, in our CRUD operations discusion, we have discussed findOne using a specific value inside ints single or multiple parameters. when we know query operators we may look for records that are more Specific.
*/

// 1. Comparison Query Operators
/*
	Includes:
		-greater Than
		-less Than
		-greater that or equal to
		-less that or equal to 
		-not equal to 
		-in
*/
/*
	-Greater Than: "$gt"
		-"$gt" find documents that have field number that are greater that a specified value
			-Syntax:
				- db.collectioName.find({field:{$gt: value}})
*/
db.users.find({age: {$gt: 76}});

/*
	-GREATER THAN or Equal to : "$gte"
		-"$gte" Finds Documents that have field number values that are greater than or equal to a specific value.
			-Syntax:
				- db.collectioName.find({field:{$gte: value}})
*/
db.users.find({age: {$gte: 76}});
/*
	-LESS THAN OPERATOR: "$lt"
		-"$lt" Finds Documents that have field number values that LESS THAN the specific value.
			-Syntax:
				- db.collectioName.find({field:{$lt: value}})
*/
db.users.find({age: {$lt: 65}});

/*
	-GREATER THAN or Equal to : "$gte"
		-"$gte" Finds Documents that have field number values that are LESS THAN OR EQUAL to a specific value.
			-Syntax:
				- db.collectioName.find({field:{$lte: value}})
*/
db.users.find({age: {$lte: 65}});

/*
	- NOT EQUAL TO OPERATOR: "$ne"
		-"$ne" Finds Documents that have field number values that are NOT EQUAL to a specific value.
			-Syntax:
				- db.collectioName.find({field:{$ne: value}})
*/
db.users.find({age: {$ne: 65}});
/*
	-IN OPERATOR: "$in"
		-"$in" Finds Documents w/ specific match criteria on one field using different value
			-Syntax:
				- db.collectioName.find({field:{$in: value}})
*/
db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
db.users.find({courses: {$in: ["HTML", "React"]}});

//  EVALUATION QUERY OPERATORS: returns data based on evaluation of either individual fields or the entire collection's documents
/*
	-REGEX OPERATORS : "$regex"
		-Regex is short for regular expression
		-They are called regular expressions because they are based on regular languages.
		-RegeX is used for matching strings.
		-it allows us to find doc that match a specific string pattern using regular expressions.
*/
/*
	-Case Sensitive query
		Syntax:
		 db.collectionName.find({field: {$regex: 'pattern'}})
*/
db.users.find({lastName: {$regex: 'A'}});
/*
	-Case Insensitive Query
		-we can run case-insensitive queries by utilizing the "i" option
		Syntax:
		 db.collectionName.find({field: {$regex: 'pattern', $options: '$optionValue'}})
*/
db.users.find({lastName: {$regex: 'A', $options: '$i'}});

/*
	mini activity

*/
db.users.find({firstName: {$regex: 'e', $options: '$i'}});


// Logical Query Operators
/*
	OR OPERATOR: $or
		- "$or" Find doc that match asingle criteria from multiple provided  search criteria.
			-Syntax:
				- db.collectionName.find({$or:[{ fieldA:valueA}, {fieldB:valaueB} ]})
*/
db.users.find({$or:[{firstName: "Neil"}, {age: "25"}]});
db.users.find({$or:[{firstName: "Neil"}, {age: {$gt:30}}]});

/*
	AND OPERATOR: "$and"
	 -"$and" find document matching multiple criteria in a single field
	 	-Syntax
	 		- db.collectionName.find({$and:[{ fieldA:valueA}, {fieldB:valaueB} ]})
*/
db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}} ]});
db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}} ]});

db.users.find({$and: [{firstName: {$regex: 'e'}}, {age: {$lte: 30}} ]});

// FIELD PROJECTION
/*
	-By default, MongiDB return the whole document especially, when dealing w/ complex documents
	-but sometime, it is not helfull to view the whole document, especially when dealing w/ complex document
	-to help w/ readability of the values return(or sometimes, bec of security reasons), we include or exclude some fields
	-in other words, we project our selected fields
*/

/*
	INCLUSION
	-allows us to include/add specifuc field only when retrieving documents
	-the value denoted is "1" to indicate that the field is included 
	-we connot do exclusion on field that uses inclusion projection
	Syntax:
		db.collectoinName.find({criteria},{field:1})
*/
db.users.find({firstName: "Jane"});

db.users.find({
	firstName: : "Jane"
},
{
	firstName: 1,
	lastName:  1,
	"contact.phone":1

}
);
// //-------------
// db.users.find({
// 	firstName: "Jane"
// },
// {
// 	firstName: 1,
// 	lastName:  1,
// 	contact: {
// 		phone: 1
// 	}

// })

/*
	returning SPECIFIC field in embedded document
		-the double quataions are important
	
*/
db.users.find({
	firstName: : "Jane"
},
{
	firstName: 1,
	lastName:  1,
	"contact.phone":1

}
);

/*
	EXEPTION TO THE INCLUESION RULE: Suppressing the ID field
	-allows us to exclude the "_id" field when retrieving documents
	-when using field projection, field inclusion and exclusion may not be used at the smae time.
	-excluding the "_id" field is the Only exceptio to this rule
		syntax:
		-db.collectionName.find({criteria},{_id:0})
*/

db.users.find({
	firstName: "jane"
	},{
	firstName: 1,
	lastName: 1,
	contact: 1,
	_id:0
});

/*
	SLICE OPERATOR: "$slice"
		-"$slice" Operator allows us to retrieve 1 element that matches the search criteria
*/
//  to demo let 1st insert an array
db.users.insert({
	namearr:[
	{namea: "juan"},
	{nameb: "tamad"},
	]
});

db.users.find({namearr: {namea: "juan"}});

// now lets us use the slice operator
db.users.find(
	{"namearr":
{
	namea:"juan"
},
{
	namearr:{$slice:1}
}

});

/*mini activity 
	
*/

db.users.find({
	firstName: {
		$regex: "s",
        $options: "i"
	}},{
	firstName: 1,
	lastName: 1,
	_id:0
	}
	
);

// EXCLUSION
/*
	-allow us to eclude or remove specific fields when retriving documents
	-the valu provided is zero to denote that the field is being exclueded
*/
db.users.find({firstName: "Jane"},
		{contact:0,
		department:0
	}
	);



